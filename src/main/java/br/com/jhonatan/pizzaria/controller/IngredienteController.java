package br.com.jhonatan.pizzaria.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.jhonatan.pizzaria.excecoes.IngredienteIvalidoException;
import br.com.jhonatan.pizzaria.modelo.entidades.Ingrediente;
import br.com.jhonatan.pizzaria.modelo.enumeracoes.CategoriaIngrediente;
import br.com.jhonatan.pizzaria.modelo.servicos.ServicoIngrediente;

@Controller
@RequestMapping("/ingredientes")
public class IngredienteController {

	@Autowired
	private ServicoIngrediente servicoIngrediente;

	@RequestMapping(method = RequestMethod.GET)
	public String listarIngredientes(Model model) {
		Iterable<Ingrediente> ingredientes = servicoIngrediente.listar();

		model.addAttribute("titulo", "Listagem de ingredientes");
		model.addAttribute("ingredientes", ingredientes);
		model.addAttribute("categorias", CategoriaIngrediente.values());

		return "ingrediente/listagem";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String salvarIngrediente(@Valid @ModelAttribute Ingrediente ingrediente, BindingResult bindingResult,
			Model model) {

		if (bindingResult.hasErrors()) {
			throw new IngredienteIvalidoException();
		} else {
			servicoIngrediente.salvar(ingrediente);
		}

		model.addAttribute("ingredientes", servicoIngrediente.listar());
		model.addAttribute("categorias", CategoriaIngrediente.values());

		return "ingrediente/tabela-ingredientes";
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<String> deletarIngrediente(@PathVariable Integer id) {
		try {
			servicoIngrediente.remover(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	@ResponseBody
	public Ingrediente pesquisarIngrediente(@PathVariable Integer id) {
		Ingrediente ingrediente = servicoIngrediente.buscar(id);
		return ingrediente;
	}
}