package br.com.jhonatan.pizzaria.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.jhonatan.pizzaria.excecoes.IngredienteIvalidoException;
import br.com.jhonatan.pizzaria.modelo.entidades.Ingrediente;
import br.com.jhonatan.pizzaria.modelo.entidades.Pizza;
import br.com.jhonatan.pizzaria.modelo.enumeracoes.CategoriaPizza;
import br.com.jhonatan.pizzaria.modelo.servicos.ServicoIngrediente;
import br.com.jhonatan.pizzaria.modelo.servicos.ServicoPizza;
import br.com.jhonatan.pizzaria.propertyeditors.IngredientePropertyEditor;

@Controller
@RequestMapping("/pizzas")
public class PizzaController {

	@Autowired
	private ServicoPizza servicoPizza;
	@Autowired
	private ServicoIngrediente servicoIngrediente;
	@Autowired
	private IngredientePropertyEditor ingredientePropertyEditor;

	@RequestMapping(method = RequestMethod.GET)
	public String listarPizzas(Model model) {

		model.addAttribute("titulo", "Listagem de pizzas");
		model.addAttribute("pizzas", servicoPizza.listar());
		model.addAttribute("categorias", CategoriaPizza.values());
		model.addAttribute("ingredientes", servicoIngrediente.listar());

		return "pizza/listagem";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String salvarPizza(@Valid @ModelAttribute Pizza pizza, BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			throw new IngredienteIvalidoException();
		} else {
			servicoPizza.salvar(pizza);
		}

		model.addAttribute("pizzas", servicoPizza.listar());

		return "pizza/tabela-pizzas";
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<String> deletarIngrediente(@PathVariable Integer id) {
		try {
			servicoPizza.remover(id);
			return new ResponseEntity<String>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	@ResponseBody
	public Pizza pesquisarPizza(@PathVariable Integer id) {
		Pizza pizza = servicoPizza.buscar(id);

		return pizza;
	}

	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		webDataBinder.registerCustomEditor(Ingrediente.class, ingredientePropertyEditor);
	}
}
