package br.com.jhonatan.pizzaria.excecoes;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class IngredienteIvalidoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

//	public IngredienteIvalidoException(String msg) {
//		super(msg);
//	}

}
