package br.com.jhonatan.pizzaria.modelo.enumeracoes;

public enum CategoriaIngrediente {
	FRIOS, SALADA, CARNE
}
