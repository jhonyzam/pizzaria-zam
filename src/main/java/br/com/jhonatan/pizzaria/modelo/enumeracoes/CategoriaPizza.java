package br.com.jhonatan.pizzaria.modelo.enumeracoes;

public enum CategoriaPizza {
	BROTINHO, MEDIA, GRANDE
}
