package br.com.jhonatan.pizzaria.modelo.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.jhonatan.pizzaria.modelo.entidades.Ingrediente;
import br.com.jhonatan.pizzaria.modelo.entidades.Pizzaria;

@Repository
public interface IngredienteRepositorio extends CrudRepository<Ingrediente, Integer> {

	public List<Ingrediente> findAllByDono(Pizzaria dono);

	public Ingrediente findByIdAndDono(Integer id, Pizzaria dono);
}
