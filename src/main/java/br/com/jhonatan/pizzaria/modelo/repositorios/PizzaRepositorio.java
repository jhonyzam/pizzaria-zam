package br.com.jhonatan.pizzaria.modelo.repositorios;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.jhonatan.pizzaria.modelo.entidades.Pizza;
import br.com.jhonatan.pizzaria.modelo.entidades.Pizzaria;

@Repository
public interface PizzaRepositorio extends CrudRepository<Pizza, Integer> {

	public List<Pizza> findAllByDono(Pizzaria dono);

	public Pizza findByIdAndDono(Integer id, Pizzaria dono);

	public List<Pizza> findAll();
}
