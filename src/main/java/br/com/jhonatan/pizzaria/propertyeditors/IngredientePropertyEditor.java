package br.com.jhonatan.pizzaria.propertyeditors;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.jhonatan.pizzaria.modelo.entidades.Ingrediente;
import br.com.jhonatan.pizzaria.modelo.repositorios.IngredienteRepositorio;

@Component
public class IngredientePropertyEditor extends PropertyEditorSupport {

	@Autowired
	private IngredienteRepositorio ingredienteRepositorio;

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		Integer id = Integer.parseInt(text);
		Ingrediente ingrediente = ingredienteRepositorio.findOne(id);
		setValue(ingrediente);
	}
}
