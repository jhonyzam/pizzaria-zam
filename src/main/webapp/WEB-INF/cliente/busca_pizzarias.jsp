<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>${titulo}</title>
		
		<c:set var="path" value="${pageContext.request.contextPath}"
			scope="request"></c:set>
		<style type="text/css">
			@IMPORT url("${path}/static/bootstrap/css/bootstrap.min.css");		
			@IMPORT url("${path}/static/bootstrap/css/bootstrap-theme.min.css");
			
			#btnBuscar{
				margin-top: 25px;
			}
		</style>
	</head>
<body>
	<div class="container">
		<jsp:include page="../menu-cliente.jsp" />

		<section>
			<div id="consulta-pizzarias" class="well">
				<div class="row">
					<div class="col-sm-4">
						<label for="pizza-pesquisa">Que pizza você quer comer hoje?</label> 
						<select id="pizza_pesquisa" class="form-control">
							<c:forEach items="${nomesPizzas}" var="nomePizza">
								<option value="${nomePizza}">${nomePizza}</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-sm-4">
						<button id="btnBuscar" class="btn btn-primary">Buscar</button>
					</div>
				</div>

			</div>

			<div id="secao-pizzarias"></div>
		</section>
	</div>
	
	<script type="text/javascript" src="${path}/static/js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="${path}/static/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="${path}/static/js/pizzarias.js"></script>
</body>
</html>