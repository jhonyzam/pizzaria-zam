<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<table
	class="table table-hover table-condensed table-striped table-bordered">
	<thead>
		<tr>
			<td style="width: 10%;">#</td>
			<td style="width: 30%;">Nome</td>
			<td style="width: 30%;">Endereço</td>
			<td style="width: 10%;">Data</td>
			<td style="width: 20%;">Email</td>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pizzarias}" var="pizzaria">
			<tr data-id="${pizzaria.id}">
				<td>${pizzaria.id}</td>
				<td>${pizzaria.nome}</td>
				<td>${pizzaria.endereco}</td>
				<td><fmt:formatDate value="${pizzaria.dataCadastro.time}" pattern="dd/MM/yyyy"/> </td>
				<td>${pizzaria.email}</td>
			</tr>
		</c:forEach>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="7">Pizzarias encontradas: <span id="qtd-pizzas">${pizzaria.size()}</span></td>
		</tr>
	</tfoot>
</table>