var btnSalvar = $("#btnSalvar");
var modal = $("#modal-ingrediente");
//
var hidID = $("#id");
var txtNome = $("#nome").val("");
var cbbCategoria = $("#categoria").val("");
//
var secao = $("#secao-ingredientes");
var qtd = $("#qtd-ingredientes");
var form = $("#form-ingrediente");

$(document).ready(function() {
	modal.on("hide.bs.modal", limparModal);

	btnSalvar.click(function() {
		var url = "ingredientes";
		var dadosIngrediente = form.serialize();
		$.post(url, dadosIngrediente).done(function(data) {
			secao.html(data);
			aplicarListener();
		}).fail(function(data) {
			console.log("ERRO");
		}).always(function() {
			modal.modal("hide");
		});
	});

	aplicarListener();
});

var limparModal = function() {
	hidID.val("");
	txtNome.val("");
	cbbCategoria.val("");
};

var aplicarListener = function() {

	$(".btnEditar").click(function() {
		var id = $(this).parents("tr").data("id");
		var url = "ingredientes/" + id;

		$.get(url, function(ingrediente) {
			console.log(ingrediente.id);
			hidID.val(ingrediente.id);
			txtNome.val(ingrediente.nome);
			cbbCategoria.val(ingrediente.categoria);

			modal.modal("show");
		});
	});

	$(".btnDeletar").click(function() {
		var id = $(this).parents("tr").data("id");
		var csrf = $("#_csrf").val();
		
		$.ajax({
			url : "ingredientes/" + id,
			type : "DELETE",
			headers : {
				"X-CSRF-TOKEN" : csrf
			},
			success : function(data) {
				var ingredientes = parseInt(qtd.text());
				$("tr[data-id='" + id + "']").remove();
				qtd.text(ingredientes - 1);
			}
		})

	});
}