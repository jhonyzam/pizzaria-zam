var cbbPizza = $("#pizza_pesquisa");
var btnBuscar = $("#btnBuscar");
var secaoPizzaria = $("#secao-pizzarias");

$(document).ready(function(){
	btnBuscar.click(function() {
		buscar();
	});
});

var buscar = function() {

	var nomePizza = cbbPizza.val();
	var url = "pizzarias/pizza" + "/" + nomePizza;
	
	$.get(url, function(view) {
		secaoPizzaria.html(view);
	});
}
