var btnSalvar = $("#btnSalvar");
var modal = $("#modal-pizza");
//
var hidID = $("#id");
var txtNome = $("#nome");
var txtPreco = $("#preco");
var cbbCategoria = $("#categoria");

//
var secao = $("#secao-pizzas");
var qtd = $("#qtd-pizzas");
var form = $("#form-pizza");

$(document).ready(function() {
	modal.on("hide.bs.modal", limparModal);

	btnSalvar.click(function() {
		var url = "pizzas";
		var dadosPizza = form.serialize();

		$.post(url, dadosPizza).done(function(data) {
			secao.html(data);
			aplicarListener();
		}).fail(function(data) {
			console.log("ERRO");
		}).always(function() {
			modal.modal("hide");
		});
	});

	aplicarListener();
});

var limparModal = function() {
	hidID.val("");
	txtNome.val("");
	txtPreco.val("");
	cbbCategoria.val("");
	$("#ingredientes").val("");
};

var aplicarListener = function() {

	$(".btnEditar").click(
			function() {
				var id = $(this).parents("tr").data("id");
				var url = "pizzas/" + id;

				$.get(url, function(pizza) {
					hidID.val(pizza.id);
					txtNome.val(pizza.nome);
					txtPreco.val(pizza.preco);
					cbbCategoria.val(pizza.categoria);

					pizza.ingredientes.forEach(function(ingrediente) {
						$("#ingredientes option[value=" + ingrediente.id + "]")
								.prop("selected", true);
					});

					modal.modal("show");
				});
			});

	$(".btnDeletar").click(function() {
		var id = $(this).parents("tr").data("id");
		var csrf = $("#_csrf").val();

		$.ajax({
			url : "pizzas/" + id,
			type : "DELETE",
			headers : {
				"X-CSRF-TOKEN" : csrf
			},
			success : function(data) {
				var pizzas = parseInt(qtd.text());
				$("tr[data-id='" + id + "']").remove();
				qtd.text(pizzas - 1);
			}
		})

	});
}